<?php
//Simple PHP Gallery v1.1
//Modified 1/15/15
class SimplePHPGallery
{
	//Config
	public $Path = "gallery"; //Path of the Images
	public $File = "art.php"; //Name of the PHP File being used
	//Gallery Home Config
	public $Preview = true; //Thumbnail preview to display over the Directory
	public $GallPerRow = 3; //Number of Gallery Directories it'll display per row
	//Gallery Display Config
	public $ThumbHeight = 150; //Thumbnail max height
	public $ThumbWidth = 150; //Thumbnail max width
	public $ThumbPerRow = 4; //Max Thumbnails per row
	
	function DisplayGalleries() //Displays the initial Gallery Directories 
	{
		?> <table align='center'>
			<tr>
		<?php
		$Dirs = scandir($this->Path); //Get the Directories from the Path
		$Count = 0; //Set a count to check for rows
		foreach ($Dirs as $Dir)
		{	
			if ($Dir === '.' || $Dir === '..') //Filter these out from displaying
				continue; 
				
			if (is_dir($this->Path ."/". $Dir)) //Check if its a directory
			{
				if ($Count>=$this->GallPerRow) //Check if Max Per Row has been hit, if so start a new one
				{
					?> </tr><tr> <?php
					$Count = 0;
				}
				?> <td><center> <?php
				if ($this->Preview == true) //Checks if Preview Thumbnails are set to display for Galleries
				{
					?> <img src='<?php echo $this->Path ."/". $Dir ."/thumbnail.jpg"; ?>'><br> <?php
				}
				$Title = $this->LinkSpaces($Dir,false)//Remove the Underscores
				?> <a href='<?php echo $this->File."?View=".$Dir;?>'><?php echo $Title; ?></a></center></td> <?php
				$Count++; //Increment Count
			}	
		}
		?> </table> <?php
	}
	
	function DisplayPictures($View=0)
	{
		//$View = $this->LinkSpaces($View,true); //Set the underscores
		$Path = $this->Path; //."/".$View;
		//Check for Groupings
		$Dirs = scandir($this->Path); //."/".$View); //Get the Directories from the Path
		foreach ($Dirs as $Dir)
		{
			if ($Dir === '.' || $Dir === '..') //Filter these out from displaying
				continue; 
			if (is_dir($Path ."/". $Dir)) //Check if its a directory
			{
				?>
					<p><table align='center' border='0'>
					<tr>
						<td colspan='<?php echo $this->ThumbPerRow ?>'><center><font size='4'><?php echo $this->LinkSpaces($Dir,false); ?></font></center></td>
					</tr>
					<tr><?php
				$SubDirs = scandir($Path."/".$Dir);	
				$Count = 0; //Set a count to check for rows
				foreach ($SubDirs as $SubDir) //Retrieves Sub-Directories to turn into Sub-Sections of the Gallery
				{
					$PictPath = $Path."/".$Dir."/".$SubDir;
					if ($Dir === '.' || $Dir === '..') //Filter these out from displaying
						continue; 
					if (is_file($Path."/".$Dir."/".$SubDir)) //Check if its a directory
					{
						if ($Count>=$this->ThumbPerRow) //Check if Max Per Row has been hit, if so start a new one
						{
							?> </tr><tr> <?php
							$Count = 0;
						}
						$Dim = $this->ResizeImage($PictPath); //Resize the image for the thumbnail
						?> <td width='150px' height='150px'>
								<div class='thumbs' align='center'>
									<a href='<?php echo $PictPath;?>' rel='lightbox[<?php echo $Dir; ?>]' title='<?php echo $Desc;?>'><img src='<?php echo $PictPath;?>' style='width: <?php echo $Dim[0];?>px; height: <?php echo $Dim[1];?>px;'></a>
								</div>
							</td>
						<?php
						$Count++;
					}
				}
				?> </table></p> <?php
			}
		}	
	}
	
	function DisplayPicture($Dir,$Pict,$Set,$Desc) //Display Individual Pictures, used instead of DisplayPictures() for a more customized setup.
	{
		$Picture = $this->Path."/".$Dir."/".$Pict;
		$Dim = $this->ResizeImage($Picture);
		?> <td width='150px' height='150px'>
				<div class='thumbs' align='center'>
					<a href='<?php echo $Picture;?>' rel='lightbox[<?php echo $Set; ?>]' title='<?php echo $Desc;?>'><img src='<?php echo $Picture;?>' style='width: <?php echo $Dim[0];?>px; height: <?php echo $Dim[1];?>px;'></a>
				</div>
			</td>
		<?php
	}
	
	function LinkSpaces($Input,$T=true) //Replaces Spaces with underscores, Input is the Directory Name, T=true is used for URL Handling, while T=false is used for displaying the Gallery's title
	{
		if ($T == true)
			$NewInput=str_replace(" ","_",$Input);
		else
			$NewInput=str_replace("_"," ",$Input);
		return $NewInput;
	}
	
	function ResizeImage($Image) //Resizes the Image so it fits into the Thumbnail without being distorted, ** parts will be cutoff
	{
		 // Constraints 
		$max_width = $this->ThumbWidth; 
		$max_height = $this->ThumbHeight; 
		list($width, $height) = getimagesize($Image); 
		$ratioh = $max_height/$height; 
		$ratiow = $max_width/$width; 
		$ratio = max($ratioh, $ratiow); 
		// New dimensions 
		$width = intval($ratio*$width); 
		$height = intval($ratio*$height); 
		return array($width,$height);
	}
}
?>
